import React from 'react';
import { View, Text, Button } from 'react-native';

const Home = ({ navigation }) => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home</Text>
      <Button
        title="Ir para a Segunda Tela"
        onPress={() => navigation.navigate('Gesto')}
      />
            <Button
        title="Ir para a Terceira Tela"
        onPress={() => navigation.navigate('Voltar')}
      />
    </View>

    
  );
};

export default Home;