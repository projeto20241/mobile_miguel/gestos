import React from 'react';
import { View, PanResponder, StyleSheet, Text, Dimensions } from 'react-native';

const Voltar= ({ navigation }) => {
  const screenWidth = Dimensions.get('window').width; // Obtém a largura da tela
  // Pan = Panning = Mover
  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true, // Identifica que iniciou um movimento
      onPanResponderMove: (event, gestureState) => { // Gesto de movimento em ação
        console.log('Movimento X:', gestureState.dx);
        console.log('Movimento Y:', gestureState.dy);
      },
      onPanResponderRelease: (event, gestureState) => { // Identifica fim do gesto
        if (gestureState.dx > screenWidth / 2) { // Ver se passou da metade da tela
          navigation.goBack();
        }
      },
    })
  ).current;

  return (
    <View
      style={styles.container}
      {...panResponder.panHandlers}
    >
      <Text style={styles.text}>Arraste da esquerda para a direita para voltar</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Voltar;