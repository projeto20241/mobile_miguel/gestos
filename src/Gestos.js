import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  Button,
  PanResponder,
  Dimensions,
  Animated,
} from "react-native";

const CountMoviment = () => {
  const [count, setCount] = useState(0);
  const screenHeight = Dimensions.get("window").height;
  const gestureThreshold = screenHeight * 0.25; // 25% da altura da tela
  const backgroundColorAnimation = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    // Efeito visual ao incrementar o contador
    Animated.sequence([
      Animated.timing(backgroundColorAnimation, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false,
      }),
      Animated.timing(backgroundColorAnimation, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }),
    ]).start();
  }, [count]);

  const interpolatedColor = backgroundColorAnimation.interpolate({
    inputRange: [0, 1],
    outputRange: ["white" ,"lightblue"],
  });

  const animatedStyle = {
    backgroundColor: interpolatedColor,
  };

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshold) {
          // Se arrastado para cima mais do que 25% da altura da tela
          setCount((prevCount) => prevCount + 1); // Usando a função de atualização do estado
        }
      },
    })
  ).current;

  return (
    <Animated.View
      style={[
        { flex: 1, alignItems: "center", justifyContent: "center" },
        animatedStyle,
      ]}
      {...panResponder.panHandlers}
    >
      <Text>Valor do estado (useState): {count}</Text>
    </Animated.View>
  );
};

export default CountMoviment;
