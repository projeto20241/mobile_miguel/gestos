import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/Home';
import Gesto from './src/Gestos'; 
import Voltar from './src/voltar'

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Gesto" component={Gesto} /> 
        <Stack.Screen name="Voltar" component={Voltar} /> 
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;